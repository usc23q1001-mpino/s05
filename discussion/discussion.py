# [SECTION] Python Class Review

class SampleClass():

	def __init__(self, year):
		self.year = year

	def show_year(self):
		print(f'The year is: {self.year}')

myObj = SampleClass(2020)

print(myObj.year)
myObj.show_year()

# [SECTION] Fundamentals 

# [SECTION] Encapsulation
# Encapsulation is a mechanism of wrapping the attributes
# and codes acting on the methods together as a single unit.
# "data hiding"

# The prefix underscore(_) is used as a warning
# for developers that means:
# Please be careful about this attribute or method
# dont use it outside the declared Class.
class Person():

	def __init__(self):
		# protected attribute _name
		self._name = "Jhon Doe"
		self._age = 0

	# Setter Method
	def set_name(self,name):
		self._name = name

	def set_age(self,age):
		self._age = age

	# Getter Method
	def get_name(self):
		print(f'Name of person: {self._name}')

	def get_age(self):
		print(f'Age of person: {self._age}')

# Object
p1 = Person()

p1.get_name()
p1.set_name("Jane Smith")
p1.get_name()

p1.get_age()
p1.set_age(5)
p1.get_age()

# Mini Exercise
# Add another protected attribute called "age"
# and create the necessary getter and setter methods


# [SECTION] Inheritance
# The transfer of the characteristics of a parent
# class to child classes are derived from it.
# "Parent-Child Relationship"
# To create an inherited class, in the className def 
# with add the parent class as the parameter of
# the child class
	
class Employee(Person):
	"""docstring for ClassName"""
	def __init__(self, employeeID):
		# super() can be used to invoke the
		# immediate parent class constructor.
		super().__init__()
		# unique attribute to the Employee class
		self._employeeID = employeeID

		# Methods of the Employee class
		def set_employeeID(self, employeeID):
			self._employeeID = employeeID

		def get_employeeID(self):
			print(f'The Employee ID is {self._employeeID}')

		# Details method
		def get_details(self):
			print(f"{self._employeeID} belongs to {self._name}")

emp1 = Employee("Emp-001")
#emp1.get_details()

# [SECTION] Polymorphism
# The method inherited from the parent class is not
# always fit for the child class
# Re-implementation/Overriding of method can be done in the child class.

# There are different methods to use polymorphmish in python

# Functions and Objects
# A functions can be created that can take any object,
#  allowing polymorphism
	
class Admin():
	def is_admin(self):
		print(True)

	def user_type(self):
		print('Admin User')

class Customer():
	def is_admin(self):
		print(False)

	def user_type(self):
		print('Customer User')

# define a test function that will take an object called obj

def test_function(obj):
	obj.is_admin()
	obj.user_type()

# create object instance to test function
user_admin = Admin()
user_customer = Customer()

test_function(user_admin)
test_function(user_customer)

# Polymorphism with Class Methods
# Pyhton uses two different class types in
# the same way

class TeamLead():
	def occupation(self):
		print('Team Lead')
	def hasAuth(self):
		print(True)

class TeamMember():
	def occupation(self):
		print('Team Member')
	def hasAuth(self):
		print(False)
		
tl1 = TeamLead()
tm1 = TeamMember()

for person in (tl1, tm1):
	person.occupation()

# Polymorphism with Inheritance
# in python defines mehtods in child class that have
# the same name as methods in the parent class
# "Method Overriding"

class Zuitt():
	def tracks(self):
		print("We are currently offering 3 tracks(developer career, pi-shape career, and short courses)")

	def num_of_hours(self):
		print("Learn Web development in 360 hours!")

class DeveloperCareer(Zuitt):
	def num_of_hours(self):
		print("Learn the basics of web dev in 240 hours!")

class PiShapedCareer(Zuitt):
	def num_of_hours(self):
		print("Learn the skills for no code app dev in 140 hours!")

class ShortCourse(Zuitt):
	def num_of_hours(self):
		print("Learn advance topics in web dev in 20 hours!")	
		
course1 = DeveloperCareer()
course2 = PiShapedCareer()
course3 = ShortCourse()

course1.num_of_hours()
course2.num_of_hours()
course3.num_of_hours()

# [SECTION] Abstraction
# An abstract class can be considered as a bluprint
# for other class 

# Abstract Base Classes (abc)
from abc import ABC, abstractclassmethod

# The class Polygon() inherits the abstract class module.
class Polygon(ABC):
	# Created an abstract method called print_number_of_sides that needs to be implemented by classes that will inherit Polygon class.
	@abstractclassmethod
	def print_number_of_sides(self):
		# This denotes that the method doesn't do anything
		pass 

class Triangle(Polygon):
	def __init__(self):
		super().__init__()

	# Since the triangle class inherited the polygon class, it must now implement the abstract method
	def print_number_of_sides(self):
		print("This polygon has 3 sides")

class Pentagon(Polygon): 
	def __init__(self):
		super().__init__()

	def print_number_of_sides(self):
		print("This polygon has 5 sides")

shape1 = Triangle()
shape2 = Pentagon()

shape1.print_number_of_sides()
shape2.print_number_of_sides()
			


			