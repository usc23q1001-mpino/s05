# Create a simple application that applies the four tenets of Object-Oriented Programming in Python

# 1. Create an abstract class called Animal that has the following abstract methods
# 	a.) eat(food)
# 	b.) make_sound()

from abc import ABC, abstractclassmethod

class Animal(ABC):
	@abstractclassmethod
	def eat(self, food):
		pass

	def make_sound(self):
		pass

# 2. Create two classes that implements the Animal class called Cat and Dog with each of the
# 	 following properties and methods:

class Cat(Animal):

# 	a.) Properties:
# 			- Name
# 			- Breed
# 			- Age
	def __init__(self, Name, Breed, Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

# 	b.) Methods:

#		- Setters
	def set_Name(self, Name):
		self._Name = Name

	def set_Breed(self, Breed):
		self._Breed = Breed

	def set_Age(self, Age):
		self._Age = Age

#		- Getters
	def get_Name(self):
		print(f"The name of the Cat is {self._Name}.")

	def get_Breed(self):
		print(f"The breed is {self._Breed}.")

	def get_Age(self):
		print(f"The age of the Cat is {self._Age} year/s old.")

#		- Implementation of abstract methods
	def eat(self, food):
		print(f"Serve me {food}")

	def make_sound(self):
		print(f"Miaow! Nyaw! Nyaaaaa!")

#		- call()
	def call(self):
		print(f"{self._Name}, come on!")


class Dog(Animal):

# 	a.) Properties:
# 			- Name
# 			- Breed
# 			- Age
	def __init__(self, Name, Breed, Age):
		super().__init__()
		self._Name = Name
		self._Breed = Breed
		self._Age = Age

# 	b.) Methods:

#		- Setters
	def set_Name(self, Name):
		self._Name = Name

	def set_Breed(self, Breed):
		self._Breed = Breed

	def set_Age(self, Age):
		self._Age = Age

#		- Getters
	def get_Name(self):
		print(f"The name of the Dog is {self._Name}.")

	def get_Breed(self):
		print(f"The breed is {self._Breed}.")

	def get_Age(self):
		print(f"The age of the Dog is {self._Age} year/s old.")

#		- Implementation of abstract methods
	def eat(self, food):
			print(f"Eaten {food}")

	def make_sound(self):
			print(f"Bark! Woof! Arf!")

#		- call()
	def call(self):
		print(f"Here {self._Name}!")




# Test Cases
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")
dog1.make_sound()
dog1.call()

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")
cat1.make_sound()
cat1.call()